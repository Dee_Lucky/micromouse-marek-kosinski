#ifndef CONFIGURATIONWINDOW_H
#define CONFIGURATIONWINDOW_H


#include <QInputDialog>
#include <QString>
#include <QtWidgets>
#include <QWidget>
#include "Maze.h"
#include "MicroMouseGraphicsView.h"
/*
 * Class which represents a configuration window. It handles setting all maze variables.
 * */
class ConfigurationWindow : public QWidget
{
public:
    ConfigurationWindow(QWidget *parent = 0);
    ~ConfigurationWindow();
private slots:
    void setWidth();
    void setHeight();
    void generateMaze();
    void MouseShouldBeBlind();
private:
    QLabel *widthIntegerLabel;
    QLabel *heightIntegerLabel;
    QPushButton* blindMouseButton;
    bool isMouseBlind=false;
};

#endif // CONFIGURATIONWINDOW_H
