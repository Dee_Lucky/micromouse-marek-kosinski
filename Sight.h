#ifndef SIGHT_H
#define SIGHT_H
#include <abstractnode.h>
#include <vector>
using namespace  std;

/*
 * Class which represents Sight. Usage of this class enables the MouseWithSight to
 * remove the nodes which are useless and not needed to visit.
 * */
class Sight
{
public:
    Sight();
    ~Sight();
    Sight(int sightDistance);
    vector<AbstractNode<int>*> LookAround(AbstractNode<int>* currentNode);
private:
    int sightDistance;
    vector<AbstractNode<int>*> visibleNodes;
    vector<AbstractNode<int>*> RecursiveLookAround(AbstractNode<int>* currentNode, int depth);

};

#endif // SIGHT_H
