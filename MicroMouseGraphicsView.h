#ifndef CUSTOMGRAPHICSVIEW_H
#define CUSTOMGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QWidget>
#include "Maze.h"
#include "Mouse.h"
class Maze;
/*
 * Class which inherits from QGraphicsView to achieve better usage of mouse input.
 * */
class MicroMouseGraphicsView : public QGraphicsView
{
public:
    MicroMouseGraphicsView ();
    MicroMouseGraphicsView (QWidget *parent );
    MicroMouseGraphicsView (QGraphicsScene *scene, QWidget *parent );

    ~MicroMouseGraphicsView();
    void SetMaze(Maze *maze);
protected:
    void mousePressEvent(QMouseEvent *event);

private:
    Maze *maze;

};

#endif // CUSTOMGRAPHICSVIEW_H
