#include "Tile.h"
#include <iostream>

using namespace std;
Tile::Tile():AbstractNode<int>()
{

}

Tile::Tile(int tileSize, int x, int y, bool isBlocked): AbstractNode<int>(x, y, isBlocked)
{
    this->tileSize = tileSize;
    this->setRect(x,y,tileSize,tileSize);
    this->setBrush(QBrush(isBlocked?Qt::black:Qt::white));
}


void Tile::BlockNode(bool toWall)
{
    AbstractNode::BlockNode(toWall);
    this->setBrush(QBrush(toWall?Qt::black:Qt::white));
}

Tile::Tile(const Tile &otherTile)
{
    this->tileSize = otherTile.tileSize;
    this->posX = otherTile.posX;
    this->posY = otherTile.posY;
    this->setRect(posX,posY,tileSize,tileSize);
}

void Tile::SetStartingTile()
{
    this->setBrush(QBrush(Qt::green));
}

void Tile::SetEndingNode()
{
    AbstractNode<int>::SetEndingNode();
    this->setBrush( QBrush(Qt::blue));
}
