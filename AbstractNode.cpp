#include "AbstractNode.h"
#include <iostream>
using namespace std;

template<typename T>
AbstractNode<T>::AbstractNode()
{
    this->isEndingNode=false;
}

template<typename T>
AbstractNode<T>::AbstractNode(T x, T y, bool isBlocked)
{
    this->posX = x;
    this->posY = y;
    this->isBlocked = isBlocked;
    this->wasVisitedByMouse = false;
    this->isEndingNode=false;
}

template<typename T>
AbstractNode<T>::AbstractNode(const AbstractNode &otherNode)
{
    this->posX = otherNode.posX;
    this->posY = otherNode.posY;
    this->isBlocked = otherNode.isBlocked;
    this->wasVisitedByMouse = otherNode.wasVisitedByMouse;
}


template<typename T>
void AbstractNode<T>::AbstractNode::SetEndingNode()
{
    this->isEndingNode=true;
}
template<typename T>
bool AbstractNode<T>::AbstractNode::IsEndingNode()
{
    return isEndingNode;
}

template<typename T>
void AbstractNode<T>::BlockNode(bool toBlocked)
{
    this->isBlocked = toBlocked;
}

template<typename T>
void AbstractNode<T>::AddNeighbour(AbstractNode* newNeighbour)
{
    neighbourNodes.push_back(newNeighbour);
}

template<typename T>
bool AbstractNode<T>::GetWasVisitedByMouse()
{
    return wasVisitedByMouse;
}
template<typename T>
void AbstractNode<T>::VisitByMouse()
{
    wasVisitedByMouse = true;
}

template<typename T>
bool AbstractNode<T>::IsNodeBlocked()
{
    return isBlocked;
}

template<typename T>
std::vector<AbstractNode<T>*> AbstractNode<T>::GetNeighbourNodes()
{
    return neighbourNodes;
}
template<typename T>
T AbstractNode<T>::GetPosX()
{
    return posX;
}
template<typename T>
T AbstractNode<T>::GetPosY()
{
    return posY;
}
