#ifndef SMARTHPATHFINDINGALGORITHM_H
#define SMARTHPATHFINDINGALGORITHM_H

#include <PathfindingAlgorithm.h>
#include <Sight.h>
#include <algorithm>
#include <Tile.h>
/*
 * Smart Pathfinding Algorithm's class. It is used by MouseWithSight. It remembers the
 * nodes it visited as well as it tries to use Sight to not visit useless nodes.
 * This class inherits from PathFindingAlgorithm. * */
template<class T>
class SmartPathfindingAlgorithm : public PathfindingAlgorithm<T>
{
private:
    vector<AbstractNode<T>*> uselessNodes;

public:
    SmartPathfindingAlgorithm() : PathfindingAlgorithm<T>(){}
    void AnalyzeNeighbourhood(AbstractNode<T>* currentNode, Sight* sight)
    {
        uselessNodes.clear();
        vector<AbstractNode<T>*> visibleNodes = sight->LookAround(currentNode);
        //From the ending, cause the last ones are the most far away from the mouse
        for(int i = visibleNodes.size()-1 ; i> 0;i--)
        {
            CheckIfNodeIsUselessNow(visibleNodes[i]);
        }
    }

    void CheckIfNodeIsUselessNow(AbstractNode<T>* node)
    {
        if(find(uselessNodes.begin(), uselessNodes.end(), node) != uselessNodes.end())
            return;
        int uselessNeighboursCount = 0;
        for(unsigned int j = 0;j < node->GetNeighbourNodes().size();j++)
        {
            if(node->GetNeighbourNodes()[j]->IsNodeBlocked() || find(uselessNodes.begin(), uselessNodes.end(), node->GetNeighbourNodes()[j]) != uselessNodes.end())
            {
                uselessNeighboursCount++;
            }

        }
        if(uselessNeighboursCount == 3 && !(node->IsEndingNode()))
        {
            uselessNodes.push_back(node);
//            ((Tile*)node)->DebugColor();
            node->VisitByMouse();
            for(unsigned int i =0; i< node->GetNeighbourNodes().size();i++)
                if(node->GetNeighbourNodes()[i]->IsNodeBlocked() && find(uselessNodes.begin(), uselessNodes.end(), node->GetNeighbourNodes()[i]) != uselessNodes.end())
                    CheckIfNodeIsUselessNow(node->GetNeighbourNodes()[i]);

        }
    }

    virtual AbstractNode<T>* GetNextNode(AbstractNode<T>* currentNode)
    {
        if(!currentNode->GetWasVisitedByMouse())
            this->alreadyVisitedNodes.push(currentNode);
        currentNode->VisitByMouse();
        bool hasUnVisitedNeighbours = false;
        AbstractNode<T>* nextNode = NULL;

        for(unsigned int i = 0 ; i < currentNode->GetNeighbourNodes().size();i++)
        {
            if(!currentNode->GetNeighbourNodes()[i]->IsNodeBlocked() && !currentNode->GetNeighbourNodes()[i]->GetWasVisitedByMouse())
            {
                hasUnVisitedNeighbours =true;
            }

            if((currentNode->GetNeighbourNodes()[i])->IsEndingNode())
            {
                nextNode = currentNode->GetNeighbourNodes()[i];
            }
        }

        if(hasUnVisitedNeighbours)
        {
            bool foundNextNode = false;
            nextNode = currentNode->GetNeighbourNodes() [rand() % currentNode->GetNeighbourNodes().size()];
            while(!foundNextNode)
            {
                while(nextNode->IsNodeBlocked() || nextNode->GetWasVisitedByMouse())
                    nextNode = currentNode->GetNeighbourNodes() [rand() % currentNode->GetNeighbourNodes().size()];
                foundNextNode = true;
            }
        }
        else
        {
            if(this->alreadyVisitedNodes.size()>0)
            {
                nextNode = this->alreadyVisitedNodes.top();
                bool allNeighboursVisited = true;
                for(unsigned int i= 0; i< nextNode->GetNeighbourNodes().size();i++)
                {
                    if(!nextNode->GetNeighbourNodes()[i]->GetWasVisitedByMouse() && !nextNode->GetNeighbourNodes()[i]->IsNodeBlocked())
                    {
                        allNeighboursVisited = false;
                    }
                }
                if(allNeighboursVisited)
                    this->alreadyVisitedNodes.pop();
            }
        }

        return nextNode;
    }
};
template class SmartPathfindingAlgorithm<int>;

#endif // SMARTHPATHFINDINGALGORITHM_H
