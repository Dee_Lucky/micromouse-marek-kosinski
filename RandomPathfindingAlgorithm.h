#ifndef RANDOMPATHFINDINGALGORITHM_H
#define RANDOMPATHFINDINGALGORITHM_H

#include <PathfindingAlgorithm.h>
#include <random>
/*
 * Random Pathfinding Algorithm's class, it does not remember any visited nodes.
 * This class inherits from PathFindingAlgorithm.
 * */
template<class T>
class RandomPathfindingAlgorithm : public PathfindingAlgorithm<T>
{
public:
    RandomPathfindingAlgorithm() : PathfindingAlgorithm<T>(){}
    virtual AbstractNode<T>* GetNextNode(AbstractNode<T>* currentNode)
    {
        AbstractNode<T>* nextNode = currentNode->GetNeighbourNodes() [rand() % currentNode->GetNeighbourNodes().size()];
        while(nextNode->IsNodeBlocked())
            nextNode = currentNode->GetNeighbourNodes() [rand() % currentNode->GetNeighbourNodes().size()];
        return nextNode;
    }
};
template class RandomPathfindingAlgorithm<int>;
#endif // RANDOMPATHFINDINGALGORITHM_H
