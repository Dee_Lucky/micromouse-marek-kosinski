#include "mousewithsight.h"


MouseWithSight::MouseWithSight():Mouse()
{
    pathfindingAlgorithm = new SmartPathfindingAlgorithm<int>();
}

MouseWithSight::~MouseWithSight()
{
    delete pathfindingAlgorithm;
    delete sight;
}

MouseWithSight::MouseWithSight(int x, int y, int tileSize): Mouse(x,y,tileSize)
{
    pathfindingAlgorithm = new SmartPathfindingAlgorithm<int>();
    sight = new Sight(3);
}

void MouseWithSight::MovementUpdate()
{
    ((SmartPathfindingAlgorithm<int>*)pathfindingAlgorithm)->AnalyzeNeighbourhood(currentNode, sight);
    AbstractNode<int>* nextNode = pathfindingAlgorithm->GetNextNode(currentNode);
    if(nextNode!=NULL)
        Move(nextNode);
}
