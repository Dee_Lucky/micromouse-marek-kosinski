#ifndef MOUSEWITHSIGHT_H
#define MOUSEWITHSIGHT_H


#include <mouse.h>
#include <Sight.h>
/*
 * Class which represents mouse with sight. It uses simple pathfinding to find the way to the exit.
 * It contains instance of Sight.
 * */
class MouseWithSight: public Mouse
{
public:
    MouseWithSight();
    virtual ~MouseWithSight();
    MouseWithSight(int x, int y, int tileSize);
    virtual void MovementUpdate();

private:
    Sight* sight;

};

#endif // MOUSEWITHSIGHT_H
