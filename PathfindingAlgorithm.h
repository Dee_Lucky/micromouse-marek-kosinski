#ifndef PATHFINDINGALGORITHM_H
#define PATHFINDINGALGORITHM_H
#pragma once
#include <abstractnode.h>
#include <stack>
using namespace std;
/*
 * Main class which represents PathfindingAlgorithm. Which is used as the name says to path finding.
 * */
template<class T>
class PathfindingAlgorithm
{
public:
    PathfindingAlgorithm();
    virtual ~PathfindingAlgorithm();
    void SetStartingAndEndingNode(AbstractNode<T>* start, AbstractNode<T>* end);
    virtual AbstractNode<T>* GetNextNode(AbstractNode<T>* currentNode)
    {
        return NULL;
    }

protected:
    AbstractNode<T>* startingNode;
    AbstractNode<T>* endingNode;
    stack<AbstractNode<T>*> alreadyVisitedNodes;
};
template class PathfindingAlgorithm<int>;
#endif // PATHFINDINGALGORITHM_H
