#include "BlindMouse.h"


BlindMouse::BlindMouse():Mouse()
{
    pathfindingAlgorithm = new RandomPathfindingAlgorithm<int>();
}

BlindMouse::BlindMouse(int x, int y, int tileSize): Mouse(x,y,tileSize)
{
    pathfindingAlgorithm = new RandomPathfindingAlgorithm<int>();
}

void BlindMouse::MovementUpdate()
{
    AbstractNode<int>* nextNode = pathfindingAlgorithm->GetNextNode(currentNode);
    Move(nextNode);
}
