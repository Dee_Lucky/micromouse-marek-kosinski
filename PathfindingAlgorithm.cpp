#include "PathfindingAlgorithm.h"

template<typename T>
PathfindingAlgorithm<T>::PathfindingAlgorithm()
{
    startingNode = NULL;
    endingNode =NULL;
}

template<typename T>
PathfindingAlgorithm<T>::~PathfindingAlgorithm()
{
    delete startingNode;
    delete endingNode;
    while(!alreadyVisitedNodes.empty())
    {
        AbstractNode<T>* node = alreadyVisitedNodes.top();
        alreadyVisitedNodes.pop();
        delete node;
    }
}

template<typename T>
void PathfindingAlgorithm<T>::SetStartingAndEndingNode(AbstractNode<T>* start, AbstractNode<T>* end)
{
    this->startingNode = start;
    this->endingNode = end;
}
