#ifndef TILE_H
#define TILE_H


#include <AbstractNode.h>
#include <QBrush>
/*
 * Class which represents Tile. It inherits from AbstractNode as well as QGraphicsRectItem.
 * Because of that it can be simply drawn into the scene and used as abstract node.
 * */
class Tile : public AbstractNode<int>, public QGraphicsRectItem
{
public:
    Tile ();
    Tile(int tileSize, int posX, int posY, bool isBlocked);
    Tile(const Tile &otherTile);
    virtual void BlockNode(bool toWall);
    void SetStartingTile();
    virtual void SetEndingNode();
private:
    int tileSize;
};

#endif // TILE_H
