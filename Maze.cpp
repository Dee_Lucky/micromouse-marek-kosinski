#include "Maze.h"
#include <QDebug>

#include <iostream>
#include <string>
#include <time.h>
#include <stdlib.h>
#include <memory.h>
#include <stdint.h>
#include <random>

Maze::Maze(QGraphicsScene *parent):QGraphicsScene(parent)
{

}

Maze::~Maze()
{
    delete text;
    delete timer;
    delete mouse;
    for(int i= 0; i< tileVector.size();i++)
        for(int j=0; j< tileVector[i].size();j++)
            delete tileVector[i][j];

    delete startingTile;
    delete endingTile;
    delete view;
}

Maze::Maze(QGraphicsScene *parent, MicroMouseGraphicsView *view, int blockSize, int width, int height) : QGraphicsScene(parent)
{
    this->startingTile = NULL;
    this->endingTile = NULL;
    this->text = new QGraphicsTextItem("");
    this->blockSize = blockSize;
    this->mazeHeight = height;
    this->mazeWidth= width;
    this->setSceneRect(0,0,blockSize*width, blockSize*height);
    this->view = view;
    this->view->SetMaze(this);
    this->view->setScene(this);
    InitializeMaze();
    this->addItem(text);
    this->view->show();
}
void Maze::ShouldMouseBeBlind(bool blind)
{
    this->blindMouse = blind;
}

void Maze::MakeMouse()
{
    if(blindMouse)
        mouse = new BlindMouse(startingTile->GetPosX(), startingTile->GetPosY(), blockSize );
    else
        mouse = new MouseWithSight(startingTile->GetPosX(), startingTile->GetPosY(), blockSize );

    this->addItem(mouse);
    this->view->show();
}

void Maze::StartSimulation()
{
    mouse->StartLookingForExit(startingTile, endingTile);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(Simulation()));
    timer->start(200);
}

void Maze::Simulation()
{
    mouse->MovementUpdate();
    if(mouse->GetCurrentNode() == endingTile)
    {
        delete timer;
        SetText("WIN!");
    }
}

void Maze::TileClicked(int x , int y)
{
    int tileX = x/blockSize;
    int tileY = y/blockSize;
    if(tileX >= 1 && tileX < mazeWidth-1 && tileY >= 1 && tileY < mazeHeight-1)
    {
        if(startingTile==NULL)
        {
            tileVector[tileX][tileY]->SetStartingTile();
            startingTile = tileVector[tileX][tileY];
            SetText( "Choose where Mouse will end it's adventure!" );
        }
        else if(endingTile==NULL)
        {
            tileVector[tileX][tileY]->SetEndingNode();
            endingTile = tileVector[tileX][tileY];
            SetText("");
            GenerateMaze();
            MakeMouse();
            StartSimulation();
        }
    }
    this->view->show();
}


void Maze::SetText(string newText)
{
    QString textString = QString::fromStdString(newText);
    text->setPlainText(textString);
    text->setDefaultTextColor(Qt::red);
    text->setPos(blockSize, blockSize);
    QFont font;
    font.setBold(true);
    font.setPixelSize(blockSize);
    text->setFont(font);
    text->show();
}
vector< vector<Tile*> > Maze::GetTileVector()
{
    return tileVector;
}

void Maze::InitializeMaze()
{
    for(int i = 0 ; i <mazeWidth;i++)
    {
        vector<Tile*> row;
        for(int j = 0 ; j < mazeHeight;j ++)
        {
            bool isBorder = i ==0 || i == mazeWidth -1 || j == 0 || j == mazeHeight -1;
            Tile *newTile = new Tile(blockSize, i*blockSize, j * blockSize, isBorder);
            row.push_back(newTile);
            this->addItem(newTile);
        }
        tileVector.push_back(row);
    }

    for(int i = 1 ; i <mazeWidth-1;i++)
    {
        for(int j = 1 ; j < mazeHeight-1;j ++)
        {
            for(int k = -1; k <2 ;k+=2)
            {
                tileVector[i][j]->AddNeighbour(tileVector[i+k][j]);
            }
            for(int l = -1; l< 2;l+=2)
            {
                tileVector[i][j]->AddNeighbour(tileVector[i][j+l]);
            }
        }
    }
    SetText( "Choose where Mouse will start it's adventure!" );
}

void Maze::GenerateMaze()
{
    for(int i = 1 ; i<mazeWidth-1;i++)
    {
        for(int j=1; j< mazeHeight-1;j++)
        {
            tileVector  [i][j]->BlockNode(rand()%2==1);
        }
    }
    int g_PtX, g_PtY, e_PtX, e_PtY;

    g_PtX = startingTile->GetPosX()/blockSize;
    g_PtY = startingTile->GetPosY()/blockSize;

    e_PtX = endingTile->GetPosX()/blockSize;
    e_PtY = endingTile->GetPosY()/blockSize;

    bool shouldMoveToRight =e_PtX>g_PtX;
    bool shouldMoveUp = e_PtY>g_PtY;

    int tempX = g_PtX ;
    int tempY = g_PtY;

    int stepsX =  (shouldMoveToRight)?(e_PtX - g_PtX):(g_PtX - e_PtX);
    int stepsY = (shouldMoveUp)?(e_PtY - g_PtY):(g_PtY-e_PtY);
    int steps = stepsX +stepsY;
    for(int i =0; i < steps;i++)
    {

        tileVector[tempX][tempY]->BlockNode(false);
        if(tempX==g_PtX && tempY == g_PtY)
            tileVector[tempX][tempY]->SetStartingTile();
        if(tempX!=e_PtX && tempY != e_PtY)
        {
            int random = rand()%2;
            if(random)
                tempX += shouldMoveToRight?1:-1;
            else
                tempY += shouldMoveUp?1:-1;
        }
        else if(tempX!=e_PtX)
        {
            tempX+=shouldMoveToRight?1:-1;
        }
        else
        {
            tempY+=shouldMoveUp?1:-1;
        }
    }
    tileVector[tempX][tempY]->BlockNode(false);
    tileVector[tempX][tempY]->SetEndingNode();
    startingTile->SetStartingTile();

}




































