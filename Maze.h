#ifndef MAZE_H
#define MAZE_H
#pragma once
#include <QInputDialog>
#include <QString>
#include <QtWidgets>
#include <QWidget>
#include <QGraphicsSimpleTextItem>
#include <QGraphicsTextItem>
#include <QFont>
#include <QPainter>
#include <QApplication>
#include <vector>
#include <string>
#include "Tile.h"
#include "MicroMouseGraphicsView.h"
#include "blindmouse.h"
#include "mousewithsight.h"
#include <iostream>
using namespace std;
class MicroMouseGraphicsView;
/*
 *  Class which represents maze. It is main class of the simulation,
 *  in which the timer is simulating the Micromouse every 0.2ms.
 *  In this class the instance of Mouse is being created as well as generation of Maze.
 * */
class Maze : public QGraphicsScene
{
    Q_OBJECT
public:
    Maze(QGraphicsScene *parent = 0);
    Maze(QGraphicsScene *parent, MicroMouseGraphicsView *view, int blockSize, int width, int height);
    ~Maze();
    void TileClicked(int x , int y);
    void ShouldMouseBeBlind(bool blind);
    vector< vector<Tile*> > GetTileVector();
private:
    QGraphicsTextItem* text;
    QTimer* timer;
    Tile* startingTile, *endingTile;
    Mouse* mouse;
    int blockSize, mazeWidth, mazeHeight;
    vector< vector<Tile*> > tileVector;
    MicroMouseGraphicsView *view;
    bool blindMouse;
    void StartSimulation();
    void MakeMouse();
    void GenerateMaze();
    void InitializeMaze();
    void SetText(string newText);
private slots:
    void Simulation();

};

#endif // MAZE_H
