#ifndef ABSTRACTNODE_H
#define ABSTRACTNODE_H

#include <QGraphicsRectItem>
#include <vector>
/*
 * Class which represents a node in graph. Used for pathfinding.
 * */
template<class T>
class AbstractNode
{
protected:
    T posX;
    T posY;
    std::vector<AbstractNode*> neighbourNodes;
    bool isBlocked;
    bool wasVisitedByMouse;
    bool isEndingNode;
public:
    AbstractNode();
    AbstractNode(T posX, T posY, bool isBlocked);
    AbstractNode(const AbstractNode &otherNode);
    std::vector<AbstractNode<T>*> GetNeighbourNodes();

    void AddNeighbour(AbstractNode* newNeighbour);
    bool IsNodeBlocked();
    bool GetWasVisitedByMouse();
    void VisitByMouse();
    void SetEndingNode();
    bool IsEndingNode();

    virtual void BlockNode(bool toBlocked);
    T GetPosX();
    T GetPosY();
};
template class AbstractNode<int>;

#endif // ABSTRACTNODE_H

