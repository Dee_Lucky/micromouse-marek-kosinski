#ifndef BLINDMOUSE_H
#define BLINDMOUSE_H

#include <mouse.h>
/*
 * Class which represents a blind mouse. Uses random movement, does not remember visited nodes.
 * */
class BlindMouse : public Mouse
{

public:
    BlindMouse();
    BlindMouse(int x, int y, int tileSize);

    virtual void MovementUpdate();
};

#endif // BLINDMOUSE_H
