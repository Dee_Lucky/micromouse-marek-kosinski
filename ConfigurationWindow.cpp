
#include <QtWidgets>
#include "configurationwindow.h"

#define MESSAGE \
    ConfigurationWindow::tr("<p>Message boxes have a caption, a text, " \
               "and any number of buttons, each with standard or custom texts." \
               "<p>Click a button to close the message box. Pressing the Esc button " \
               "will activate the detected escape button (if any).")
#define MESSAGE_DETAILS \
    ConfigurationWindow::tr("If a message box has detailed text, the user can reveal it " \
               "by pressing the Show Details... button.")

ConfigurationWindow::~ConfigurationWindow()
{
    delete widthIntegerLabel;
    delete heightIntegerLabel;
    delete blindMouseButton;
}

ConfigurationWindow::ConfigurationWindow(QWidget *parent): QWidget(parent)
{
    QVBoxLayout *verticalLayout;
    if (QGuiApplication::styleHints()->showIsFullScreen() || QGuiApplication::styleHints()->showIsMaximized()) {
        QHBoxLayout *horizontalLayout = new QHBoxLayout(this);
        QGroupBox *groupBox = new QGroupBox(QGuiApplication::applicationDisplayName(), this);
        horizontalLayout->addWidget(groupBox);
        verticalLayout = new QVBoxLayout(groupBox);
    } else {
        verticalLayout = new QVBoxLayout(this);
    }

    QToolBox *toolbox = new QToolBox;
    verticalLayout->addWidget(toolbox);

    int frameStyle = QFrame::Sunken | QFrame::Panel;

    widthIntegerLabel = new QLabel;
    widthIntegerLabel->setFrameStyle(frameStyle);
    int startingWidth = 25;
    widthIntegerLabel->setText(tr("%1").arg(startingWidth));
    QPushButton *widthIntegerButton = new QPushButton(tr("Maze width : "));

    blindMouseButton = new QPushButton(tr("Mouse With Sight"));
    heightIntegerLabel = new QLabel;
    heightIntegerLabel->setFrameStyle(frameStyle);
    int startingHeight = 25;
    heightIntegerLabel->setText(tr("%1").arg(startingWidth));
    QPushButton *heightIntegerButton = new QPushButton(tr("Maze height : "));
    QPushButton *generateMazeButton = new QPushButton(tr("Generate Maze !"));

    connect(widthIntegerButton, &QAbstractButton::clicked, this, &ConfigurationWindow::setWidth);
    connect(heightIntegerButton, &QAbstractButton::clicked, this, &ConfigurationWindow::setHeight);
    connect(generateMazeButton, &QAbstractButton::clicked, this, &ConfigurationWindow::generateMaze);
    connect(blindMouseButton ,&QAbstractButton::clicked, this, &ConfigurationWindow::MouseShouldBeBlind);

    QWidget *page = new QWidget;
    QGridLayout *layout = new QGridLayout(page);

    layout->setColumnStretch(1, 1);
    layout->setColumnMinimumWidth(1, 50);
    layout->addWidget(widthIntegerButton, 0, 0);
    layout->addWidget(widthIntegerLabel, 0, 1);
    layout->addWidget(heightIntegerButton, 1, 0);
    layout->addWidget(heightIntegerLabel, 1, 1);

    layout->addWidget(blindMouseButton,2,0);

    layout->addWidget(generateMazeButton, 3, 0);
    layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Ignored, QSizePolicy::MinimumExpanding), 5, 0);

    page->show();
    setWindowTitle(QGuiApplication::applicationDisplayName());
}

void ConfigurationWindow::MouseShouldBeBlind()
{
    isMouseBlind = !isMouseBlind;
    blindMouseButton->setText(isMouseBlind?"Blind Mouse":"Mouse With Sight");
}

void ConfigurationWindow::setWidth()
{
    bool ok;
    int i = QInputDialog::getInt(this, tr("Set width"), tr("Width:"), 10, 10, 200, 1, &ok);
    if (ok)
        widthIntegerLabel->setText(tr("%1").arg(i));
}

void ConfigurationWindow::setHeight()
{
    bool ok;
    int i = QInputDialog::getInt(this, tr("Set height"), tr("Height:"), 10, 10, 200, 1, &ok);
    if (ok)
        heightIntegerLabel->setText(tr("%1").arg(i));
}

void ConfigurationWindow::generateMaze()
{
    int blockSize = 30;
    MicroMouseGraphicsView *view = new MicroMouseGraphicsView();
    Maze *maze = new Maze(NULL, view, blockSize ,widthIntegerLabel->text().toInt(), heightIntegerLabel->text().toInt());
    maze->ShouldMouseBeBlind(isMouseBlind);
}

