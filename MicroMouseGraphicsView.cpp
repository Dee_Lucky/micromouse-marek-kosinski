#include "MicroMouseGraphicsView.h"

MicroMouseGraphicsView::MicroMouseGraphicsView()
{

}

MicroMouseGraphicsView::MicroMouseGraphicsView(QWidget *parent) : QGraphicsView(parent)
{

}


MicroMouseGraphicsView::MicroMouseGraphicsView(QGraphicsScene *scene, QWidget *parent) : QGraphicsView(scene,parent)
{

}

MicroMouseGraphicsView::~MicroMouseGraphicsView()
{
    delete maze;
}


void MicroMouseGraphicsView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        int x = event->pos().x();
        int y = event->pos().y();

        maze->TileClicked(x,y);

    }
}



void MicroMouseGraphicsView::SetMaze(Maze *maze)
{
    this->maze = maze;
}
