#ifndef MOUSE_H
#define MOUSE_H
#pragma once
#include <randompathfindingalgorithm.h>
#include <SmartPathfindingAlgorithm.h>
#include <abstractnode.h>
#include <QGraphicsRectItem>
#include <QtGlobal>
#include <QBrush>
/*
 * Main class of Mouse. It handles the movement and basic variables.
 * */
class Mouse : public QGraphicsRectItem
{
public:
    Mouse();
    Mouse(int x, int y, int tileSize);
    virtual ~Mouse();
    virtual void MovementUpdate();
    void StartLookingForExit(AbstractNode<int>* start, AbstractNode<int>* end);
    AbstractNode<int>* GetCurrentNode();
protected:
    int x, y, tileSize;
    PathfindingAlgorithm<int> *pathfindingAlgorithm;
    void Move(AbstractNode<int>* node);
    AbstractNode<int>* currentNode;
};

#endif // MOUSE_H
