//#include "rasterwindow.h"
#include <QGraphicsView>
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <ConfigurationWindow.h>
#include <iostream>

int main(int argc, char **argv)
{

    QApplication app(argc, argv);
    ConfigurationWindow configurationWindow;

    if (!QGuiApplication::styleHints()->showIsFullScreen() && !QGuiApplication::styleHints()->showIsMaximized()) {
        const QRect availableGeometry = QApplication::desktop()->availableGeometry(&configurationWindow);
        configurationWindow.resize(250, 350);
        configurationWindow.move((availableGeometry.width() - configurationWindow.width()) / 3,
                    (availableGeometry.height() - configurationWindow.height()) / 2);
    }
    return app.exec();
    return 0;
}
