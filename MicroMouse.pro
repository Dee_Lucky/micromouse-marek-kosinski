QT += widgets
QT += gui

CONFIG += c++11 console
CONFIG -= app_bundle
CONFIG -= console
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    MicroMouseGraphicsView.cpp \
    Tile.cpp \
    Maze.cpp \
    ConfigurationWindow.cpp \
    AbstractNode.cpp \
    Mouse.cpp \
    PathfindingAlgorithm.cpp \
    BlindMouse.cpp \
    MouseWithSight.cpp \
    Sight.cpp
target.path = $$[QT_INSTALL_EXAMPLES]/gui/rasterwindow
INSTALLS += target

HEADERS += \
    MicroMouseGraphicsView.h \
    Tile.h \
    Maze.h \
    ConfigurationWindow.h \
    AbstractNode.h \
    Mouse.h \
    PathfindingAlgorithm.h \
    RandomPathfindingAlgorithm.h \
    BlindMouse.h \
    MouseWithSight.h \
    Sight.h \
    SmartPathfindingAlgorithm.h

DISTFILES +=
