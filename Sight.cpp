#include "Sight.h"
#include <algorithm>

Sight::Sight()
{
    sightDistance =0;
}
Sight::~Sight()
{
    for(int i=0;i<visibleNodes.size();i++)
        delete visibleNodes[i];
}

Sight::Sight(int sightDistance)
{
    this->sightDistance = sightDistance;
}


vector<AbstractNode<int>*> Sight::RecursiveLookAround(AbstractNode<int>* currentNode, int depth)
{
    vector<AbstractNode<int>*> tempNodes;
    if(depth>0)
    {
        tempNodes = currentNode->GetNeighbourNodes();
        depth--;
        for(unsigned int i = 0 ; i < tempNodes.size();i++)
        {
            if(find(visibleNodes.begin(), visibleNodes.end(), tempNodes[i]) == visibleNodes.end())
            {
                if(!tempNodes[i]->IsNodeBlocked())
                {
                    visibleNodes.push_back(tempNodes[i]);
                    vector<AbstractNode<int>*> deeperNodes = RecursiveLookAround(tempNodes[i], depth);
                    for(unsigned int j = 0 ; j < deeperNodes.size(); j++)
                    {
                        if(!deeperNodes[j]->IsNodeBlocked())
                            visibleNodes.push_back(deeperNodes[j]);
                    }
                }
            }
        }
    }
    return visibleNodes;
}

vector<AbstractNode<int>*> Sight::LookAround(AbstractNode<int>* currentNode)
{
    visibleNodes.clear();
    visibleNodes.push_back(currentNode);
    visibleNodes = RecursiveLookAround(currentNode, this->sightDistance);
    return visibleNodes;
}



