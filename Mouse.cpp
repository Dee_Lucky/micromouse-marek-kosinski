#include "Mouse.h"
#include "QImageReader"
#include <iostream>
using namespace  std;
Mouse::Mouse()
{
    this->x = 0;
    this->y = 0;
    pathfindingAlgorithm = NULL;
}
Mouse::~Mouse()
{
    delete pathfindingAlgorithm;
    delete currentNode;
}
Mouse::Mouse(int x, int y, int tileSize)
{
    QImageReader reader;
    this->x = x;
    this->y = y;
    pathfindingAlgorithm = NULL;
    this->tileSize = tileSize;
    this->setRect(x+tileSize/4, y+tileSize/4, tileSize/2, tileSize/2);
    this->setBrush(QBrush(Qt::yellow));
}

void Mouse::Move(AbstractNode<int>* node)
{
    currentNode = node;
    this->x = node->GetPosX();
    this->y = node->GetPosY();
    this->setRect(x+tileSize/4, y+tileSize/4, tileSize/2, tileSize/2);
}

AbstractNode<int>* Mouse::GetCurrentNode()
{
    return currentNode;
}
void Mouse::StartLookingForExit(AbstractNode<int> *start, AbstractNode<int> *end)
{
    if(pathfindingAlgorithm!=NULL)
    {
        currentNode = start;
        pathfindingAlgorithm->SetStartingAndEndingNode(start, end);
    }
}

void Mouse::MovementUpdate()
{

}
